#include "TextureManager.hpp"

SDL_Texture*    TextureManager::LoadTexture(const char *fileName)
{
    SDL_Surface* tmpSurface = IMG_Load(fileName);
    SDL_Texture* tex = SDL_CreateTextureFromSurface(Game::renderer, tmpSurface);
    SDL_FreeSurface(tmpSurface);

    return (tex);
}

SDL_Texture*    TextureManager::LoadColor(int r, int g, int b, int a)
{
    SDL_Texture* colortex = SDL_CreateTexture(Game::renderer, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 32, 32);
    SDL_SetTextureColorMod(colortex, r, g, b);
    return colortex;
}

void            TextureManager::Draw(SDL_Texture* tex, SDL_Rect src, SDL_Rect dest, SDL_RendererFlip flip)
{
    SDL_RenderCopyEx(Game::renderer, tex, &src, &dest, 0.0, NULL, flip);
}