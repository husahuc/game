#ifndef TextureManager_hpp
#define TextureManager_hpp
#include "game.hpp"

class TextureManager {

public:
    static SDL_Texture* LoadTexture(const char *fileName);
    static SDL_Texture* LoadColor(int r, int g, int b, int a);
    static void Draw(SDL_Texture* tex, SDL_Rect src, SDL_Rect dest, SDL_RendererFlip flip);
};
#endif