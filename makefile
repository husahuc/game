CC=g++ -std=c++14
LIB=`sdl2-config --cflags --libs` -lSDL2_image
FILES=src/main.cpp src/game.cpp src/TextureManager.cpp src/GameObject.cpp \
src/Map.cpp src/Vector2D.cpp src/Collision.cpp \
src/ECS/ECS.cpp
NAME=graphics

all: $(NAME)

$(NAME): $(FILES)
	@$(CC) $(FILES) $(LIB) -o $@

fclean:
	@rm -rf $(NAME)

re: fclean all